xquery version "3.1";

import module namespace tgconnect="http://textgrid.info/namespaces/xquery/tgconnect" at "/db/apps/textgrid-connect/tg-connect.xql";

declare variable $sid external;
declare variable $uri external;
declare variable $pos external;
declare variable $count external;

declare function local:getColor($pos, $count) {
    let $steps := floor( 510 div $count)
    let $this := $pos * $steps
        return
            if( $this lt 255 )
            then "255;" || $this || ";0"
            else 510 - $this || ";255;0"
    };

let $color := local:getColor($pos, $count)
let $systemOut := util:log-system-out( "&#27;[48;2;"|| $color ||"m&#27;[38;2;0;0;0m " || $uri || " &#27;[0m" )
return
  tgconnect:publish(    $uri,
                        $sid,
                        "data",
                        "admin",
                        "",
                        "textgrid",
                      true()
    )
