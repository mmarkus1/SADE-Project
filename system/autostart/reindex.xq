xquery version "3.1";

let $doc := doc( "/db/system/config/db/sade-projects/textgrid/data/xml/data/collection.xconf" )/*
let $tmp :=     xmldb:store( "/db", "tmp-collection.xconf", $doc )
let $newdoc := doc( $tmp )
let $log := util:log-system-out("Autostart: »Hello.«")
return
    (
        xmldb:remove( "/db/system/config/db/sade-projects/textgrid/data/xml/data", "collection.xconf"),
        xmldb:store( "/db/system/config/db/sade-projects/textgrid/data/xml/data", "collection.xconf", $newdoc ),
        xmldb:remove( "/db", "tmp-collection.xconf" )
    ) , xmldb:reindex("/db/sade-projects/textgrid/data/xml/data")
