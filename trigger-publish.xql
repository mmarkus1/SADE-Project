xquery version "3.1";

import module namespace tgconnect="http://textgrid.info/namespaces/xquery/tgconnect" at "/db/apps/textgrid-connect/tg-connect.xql";
import module namespace tgclient="http://textgrid.info/namespaces/xquery/tgclient" at "/db/apps/textgrid-connect/tgclient.xqm";

declare namespace tgmd="http://textgrid.info/namespaces/metadata/core/2010";

(: file path pointing to the eXist-db installation directory :)
declare variable $home external;
(: path to the directory containing the unpacked .xar package :)
declare variable $dir external;
(: the target collection into which the app is deployed :)
declare variable $target external;

(: using static pathes since $target and $dir serve some errors in 3.1.1 :)

declare variable $configdoc := doc( "/db/sade-projects/textgrid/config.xml" );

declare variable $config := map:new(for $param in $configdoc//param
                              return map:entry(string($param/@key), string($param))
                              );
declare variable $sid :=
  tgclient:getSid(
    $config("textgrid.webauth"),
    $config("textgrid.authZinstance"),
    $config("textgrid.user"),
    $config("textgrid.password")
  );



for $uri in ( "textgrid:1zzdp" )
where starts-with($uri, "textgrid:")
    let $response :=
      try {
        (: call via httpclient, as Lab does, because function call not working on app deployment for unknown reason :)
        httpclient:post(
          xs:anyURI("http://localhost:8080/exist/apps/textgrid-connect/publish/textgrid/process/?sid="
          || $sid ||"&amp;surface=&amp;uri="|| $uri ||"&amp;user="||$config("sade.user")||"&amp;password="||$config("sade.password")),
           "",
           false(),
           ())
       (: should be
            tgconnect:publish(  $uri,
                        $sid,
                        "data",
                        "admin",
                        "",
                        "textgrid")
                        :)
    } catch * { <error code="{$err:code}" timestamp="{current-dateTime()}">{$err:description}</error> }
return
  <report uri="{$uri}" start="{current-dateTime()}">{$response}</report>
