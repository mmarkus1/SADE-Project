xquery version "3.0";
declare namespace tgmd="http://textgrid.info/namespaces/metadata/core/2010";

let $metacol := collection('/db/sade-projects/textgrid/data/xml/meta')

let $seq1 := distinct-values($metacol//tgmd:object[descendant::tgmd:format = 'text/xml']//tgmd:title)
let $xml :=
<navigation>
{for $item in $seq1 
    order by $item
    return
        <object type="text/xml" uri="{
            $metacol//tgmd:object
                [descendant::tgmd:format = 'text/xml']
                [descendant::tgmd:title = $item]
                [descendant::tgmd:revision = 
                    max($metacol//tgmd:object
                        [descendant::tgmd:format = 'text/xml']
                        [descendant::tgmd:title = $item]//tgmd:revision)]//tgmd:textgridUri}" title="{$item}"/>
}
{
for $item in $metacol//tgmd:object[descendant::tgmd:format = 'image/jpeg']
    order by $item//tgmd:title/string()
    return
        <object type="image/jpeg" uri="{$item//tgmd:textgridUri}"  title="{$item//tgmd:title}"/>
}
</navigation>

return
(:    $xml:)
    xmldb:store('/db/sade-projects/textgrid','navigation-fontane.xml', $xml, 'text/xml')