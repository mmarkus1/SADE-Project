<?php

// reCaptcha Setup
// ===============

// Insert below your reCaptcha Site and Secret Keys
// Go to https://www.google.com/recaptcha/admin/create if you don't have the keys yet

$publickey = "6LdAGzcUAAAAALDwgYCtb09wDoHMyzUPXt-Tc9dw"; // Site key
$privatekey = "6LdAGzcUAAAAANfsrgQQQxhAYK6V0d8muvXO73Id"; // Secret key

// Mail Setup
// ==========

// Sender Name and <email address> separated by space
$mail_sender = 'Support <contact@architrave.eu>';
// Your Email Address where new emails will be sent to
$to_email = 'architrave@sub.uni-goettingen.de';
// Email Subject
$mail_subject = 'ARCHITRAVE Contact Request';

// Email content can be modified in the sendmail.php file.

?>
