var code = {
                type: 'component',
                componentName: 'code',
                title:'Code',
                id: 'code',
                componentState: {
                    importclass: '.fcode'
                },
                width: 30,
                isClosable: false
          },
    codecontainer = $(".fcode").clone();
    tran = {
                type: 'component',
                componentName: 'tran',
                title:'Transkription',
                id: 'tran',
                componentState: {
                  importclass: '.ftran' },
                width: 35,
                isClosable: false
              },
    facs = {
                type: 'component',
                componentName: 'facs',
                title:'Faksimile',
                id: 'facs',
                componentState: {
                    importclass: '.ffacs' },
                width: 35,
                isClosable: false
              },
    toc = {
         type: 'component',
                componentName: 'toc',
                title:'Blattübersicht',
                id: 'toc',
                componentState: {
                    importclass: '.ftoc' },
                isClosable: false
    },
    comm = {
         type: 'component',
                componentName: 'comm',
                title:'Stellenkommentar (0)',
                id: 'comm',
                componentState: {
                    importclass: '.fcomm' },
                isClosable: false
    },
   config = {
    settings: {showPopoutIcon: false, showCloseIcon: false},
    dimensions: {headerHeight: 30},
    content: [{
        type: 'row',
            content:[facs,tran,
            {
                type: "stack",
                content:
                    [code, toc, comm]

            }
            ]

    }]
};

// look for a saved state
var myLayout,
    savedState = localStorage.getItem( 'savedState' );

if( savedState !== null ) {
  	myLayout = new GoldenLayout( JSON.parse( savedState ), $('#goldenEdition')  );
} else {
  	myLayout = new GoldenLayout( config, $('#goldenEdition') );
}


// register components
myLayout.registerComponent( 'code', function( container, state ){
    container.getElement().html( '<div id="placeholder">…</div>');
    $.ajax({
        url: "get/code.html"+window.location.search,
        dataType: "text"
        }).done( function(data){
            $( "#placeholder" ).replaceWith( data );
            hljs.highlightBlock( $("code.html")[0] );
        });
});

myLayout.registerComponent( 'tran', function( container, state ){
container.getElement().html( '<div class="ftran sourceDoc"></div>');
    $.ajax({
        url: "get/trans.html"+window.location.search,
        dataType: "text"
        }).done( function(data){
            $( ".ftran" ).append( data );
            registerInfo();
            outsider();
            test4mod();
            syncScroll( $(".ffacs").parent(), $(".ftran").parent() );
        });
});

myLayout.registerComponent( 'facs', function( container, state ){
container.getElement().html( '<div class="ffacs"></div>');
    $.ajax({
        url: "get/facs.html"+window.location.search,
        dataType: "text"
        }).done( function(data){
            $( ".ffacs" ).append( data );
            syncScroll( $(".ffacs").parent(), $(".ftran").parent() );
        });
});

myLayout.registerComponent( 'toc', function( container, state ){
container.getElement().html( '<div class="ftoc"></div>');
    $.ajax({
        url: "get/toc.html"+window.location.search,
        dataType: "text"
        }).done( function(data){
            $( ".ftoc" ).append( data );
        });
});

myLayout.registerComponent( 'comm', function( container, state ){
container.getElement().html( '<div class="fcomm"></div>');
    $.ajax({
        url: "get/comm.html"+window.location.search,
        dataType: "text"
        }).done( function(data){
            $( ".fcomm" ).append( data );
            var currentTitle = container._config.title;
            var numComments = $(".fcomm").find("li").length;
            var newTitle = currentTitle.replace(/\d/g, numComments);
            container.setTitle(newTitle);
        });
});

myLayout.init();

if( localStorage.savedState ) {
    if( localStorage.savedState.indexOf("code") === -1) {
        $("#xmlBtn").toggleClass( "inactive" );
    }
    if( localStorage.savedState.indexOf("tran") === -1 ) {
        $("#transBtn").toggleClass( "inactive" );
    }
    if( localStorage.savedState.indexOf("facs") === -1 ) {
        $("#facsBtn").toggleClass( "inactive" );
    }
    if( localStorage.savedState.indexOf("toc") === -1 ) {
        $("#tocBtn").toggleClass( "inactive" );
    }
}

// save the state
myLayout.on( 'stateChanged', function(){
    var state = JSON.stringify( myLayout.toConfig() );
    localStorage.setItem( 'savedState', state );
});


$( window ).resize( function(){ myLayout.updateSize(); } );

function goldenActivate( trigger ){
    var child;
    if(trigger == 'code') { child = code; }
    else if(trigger == 'tran') { child = tran; }
    else if(trigger == 'toc') { child = toc; }
    else if(trigger == 'facs') { child = facs; };

    myLayout.root.contentItems[ 0 ].addChild(
        child
        );
    var insert;
    switch ( trigger ) {
        case 'code':
            $.ajax({
                url: "get/code.html"+window.location.search
                })
                .done( function(data){
                    myLayout.root.contentItems[0].getItemsById( trigger )[0].container.getElement().html( data );
                    hljs.highlightBlock( $("code.html")[0] );
                } );
            break;
        case 'tran':
            $.ajax({
                url: "get/trans.html"+window.location.search
                })
                .done( function(data){
                        myLayout.root.contentItems[0].getItemsById( trigger )[0].container.getElement().html( '<div class="ftran sourceDoc">' + data + "</div>" );
                        syncScroll( $(".ffacs").parent(), $(".ftran").parent() );
                        registerInfo();
                        outsider();
                        test4mod();
                } );
            break;
        case 'facs':
            $.ajax({
                url: "get/facs.html"+window.location.search
                })
                .done( function(data){
                        myLayout.root.contentItems[0].getItemsById( trigger )[0].container.getElement().html( '<div class="ffacs">' + data + "</div>" );
                        syncScroll( $(".ftran").parent(), $(".ffacs").parent() );
                } );
            break;
        default:
            console.log( "unknown trigger" );
    };
}

function goldenDestroy ( id ) {
    myLayout.root.getItemsById( id )[0].remove();
}



// toggle button
function tbtn(e){
    switch ( e ) {
            case 'code':
                $('#xmlBtn').addClass("inactive");
                break;
            default:
                console.log( "unprepared item toggle" );
        }
}


// stolen from goozbox @stackoverflow
function syncScroll(el1, el2) {
  var $el1 = $(el1);
  var $el2 = $(el2);

  // Lets us know when a scroll is organic
  // or forced from the synced element.
  var forcedScroll = false;

  // Catch our elements' scroll events and
  // syncronize the related element.
  $el1.scroll(function() { performScroll($el1, $el2); });
  $el2.scroll(function() { performScroll($el2, $el1); });

  // Perform the scroll of the synced element
  // based on the scrolled element.
  function performScroll($scrolled, $toScroll) {
    if (forcedScroll) return (forcedScroll = false);
    var percentTop = ($scrolled.scrollTop() /
        ($scrolled[0].scrollHeight - $scrolled.outerHeight())) * 100;
    var percentLeft = ($scrolled.scrollLeft() /
        ($scrolled[0].scrollWidth - $scrolled.outerWidth())) * 100;
    setScrollTopFromPercent($toScroll, percentTop);
    setScrollLeftFromPercent($toScroll, percentLeft);
  }

  // Scroll to a position in the given
  // element based on a percent.
  function setScrollTopFromPercent($el, percent) {
    var scrollTopPos = (percent / 100) *
      ($el[0].scrollHeight - $el.outerHeight());
    forcedScroll = true;
    $el.scrollTop(scrollTopPos);
  }
  function setScrollLeftFromPercent($el, percent) {
    var scrollLeftPos = (percent / 100) *
      ($el[0].scrollWidth - $el.outerWidth());
    forcedScroll = true;
    $el.scrollLeft(scrollLeftPos);
  }
}

// use the nb title to clear local storage
$("#nb-title").click(function(){
    localStorage.removeItem("savedState");
    location.reload();
});

// look for outsiders
function outsider() {
if ( $( ".surface" ).length ) {
        $( ".surface" ).each(function(){
        // check for an addition outside of the surface
        var minOffset = $( this ).offset().left;
        $('*[style*="left:-"]').each(function () {
          var thisOffset = $(this).offset().left;
          var newVal = minOffset - thisOffset;
          if ( thisOffset < minOffset ) {
            $(this).css('margin-left', newVal + 'px');
          }
        }
        );
        var minOffset = $( this ).offset().top;
        console.log(minOffset);
        $('*[style*="left:-"]').each(function () {
          var thisOffset = $(this).offset().top;
          var newVal = minOffset - thisOffset;
          if (thisOffset < minOffset) {
            $(this).css('margin-left', newVal + 'px');
          }
        }
        );
        });
    }
}

function test4mod(){
    var $allMods = $("[class*='mod-seq']"), len = $allMods.length, c=0;
    $("#seqBtn").click( function(){
        $allMods.removeClass("hidden").eq(++c%len).addClass("hidden");
    } );
}
