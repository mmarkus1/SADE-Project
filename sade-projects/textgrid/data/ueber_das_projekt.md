# ÜBER DAS PROJEKT

### Genetisch-kritische und kommentierte Hybrid-Edition von Theodor Fontanes Notizbüchern basierend auf einer Virtuellen Forschungsumgebung

Theodor Fontanes 67 Notizbücher sind das letzte noch unveröffentlichte größere Textkorpus des Autors, es gibt bislang nur wenige, den textkritischen Standards nicht genügende Teilpublikationen. Von 1859 bis Ende der 1880er Jahre hat Fontane Notizbücher geführt, die unterschiedliche Notate enthalten: Tagebuchaufzeichnungen, Briefkonzepte, poetische Pläne, Vortragsmitschriften, Entwürfe zu Theater- und Kunstkritiken, Buchexzerpte sowie Notizen und Zeichnungen, die während der Ausflüge durch die Mark Brandenburg und auf weiteren Reisen entstanden sind. Hinzu kommen Alltagsnotizen wie To-do-Listen und Zugabfahrtspläne, Lektüre- und Briefempfängerlisten.



 

Die editorische Vernachlässigung hat zur Folge, dass eine Rezeption der Notizbücher ausgeblieben ist und die Aufzeichnungen für die Entstehungsgeschichte und Textgenese der Werke Fontanes nur gelegentlich ausgewertet wurden. Die genetisch-kritische Hybrid-Edition wird erstmals alle Notizbuchniederschriften ermitteln, transkribieren, codieren, kommentieren und veröffentlichen. Im Unterschied zu den bisherigen, an inhaltlichen Kriterien orientierten Einzelpublikationen stellt das Editionskonzept die komplexe Überlieferung mit ihren materialen und medialen Kennzeichen in den Mittelpunkt.





Die Hybrid-Ausgabe besteht aus zwei komplementären Teilen, die in abgestufter Weise die Materialität visualisieren und dokumentenorientierte, chronologische und teleologische Zugriffe ermöglichen sowie einen linearen les- und zitierbaren Text und Kommentar herstellen: Die digitale Edition im Fontane-Notizbuch-Portal wird alle Notizbuchaufzeichnungen in synoptischer Darstellung von Digitalisat und diplomatischer Transkription sowie einen historisch-kritisch edierten Text mit textkritischem Apparat und Kommentaren unter Open-Access-Lizenz CC-BY-NC-ND 4.0 international veröffentlichen. Die Buch-Edition, die im de-Gruyter-Verlag erscheint, wird die historisch-kritische Textfassung mit Apparat und Kommentaren umfassen. Die genetisch-kritische Hybrid-Edition wird neue Impulse für die werkgenetische, literatur- und kulturwissenschaftliche sowie mentalitätsgeschichtliche Forschung geben; sie wird auch ein Modell für weitere Notizbuch-Editionen mit ähnlich schwierigem Überlieferungskontext zur Verfügung stellen.



 

Die philologische Editionsarbeit wird durch digitale Methoden und den Einsatz der Virtuellen Forschungsumgebung TextGrid maßgeblich unterstützt. Für die komplexen Anforderungen der Notizbuch-Edition werden die Dienste und Werkzeuge der Virtuellen Forschungsumgebung genutzt, angepasst und zum Teil weiterentwickelt. TextGrid fördert die Zusammenarbeit mit anderen Forschungsvorhaben durch den Austausch von Zwischenergebnissen und ermöglicht eine Nachnutzung der Forschungsdaten unter der Bewahrung der geistigen Urheberschaft der geleisteten Arbeit. Eine langfristige Verfügbarkeit der Forschungsdaten wird durch die digitale Publikation im TextGrid-Repository gewährleistet.



 

Die Edition entsteht an der [Theodor Fontane-Arbeitsstelle](http://www.uni-goettingen.de/de/154180.html) der Universität Göttingen  in Kooperation mit der Niedersächsischen Staats- und Universitätsbibliothek Göttingen, [Abteilung Forschung & Entwicklung](http://www.sub.uni-goettingen.de/projekte-forschung/forschung-entwicklung) und [Gruppe Metadaten und Datenkonversion](http://www.sub.uni-goettingen.de/kontakt/abteilungen-a-z/abteilungs-und-gruppendetails/abteilunggruppe/metadaten-und-datenkonversion/).

