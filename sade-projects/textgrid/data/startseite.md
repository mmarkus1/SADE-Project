Die genetisch-kritische und kommentierte Hybrid-Edition von Theodor Fontanes Notizbüchern ist ein von der [ Deutschen Forschungsgemeinschaft](http://gepris.dfg.de/gepris/projekt/193395716 ) gefördertes Projekt. Sie entsteht an der [ Theodor Fontane-Arbeitsstelle](http://www.uni-goettingen.de/de/154180.html ) der Universität Göttingen und an der [ Niedersächsischen Staats- und Universitätsbibliothek Göttingen](http://www.sub.uni-goettingen.de ).





Aufgrund ihrer komplexen Beschaffenheit galten Fontanes 67 Notizbücher bislang als nicht edierbar. Das Zusammenwirken philologischer und digitaler Methoden sowie die Arbeit in der Virtuellen Forschungsumgebung [ TextGrid](http://www.textgrid.de/ ) ermöglicht nun die erste kommentierte Gesamtedition.

 

